#!/usr/bin/env php
<?php

// parse $excep_file file to memorise file to transform (bashrc -> .bashrc)
// $prefix is a prefix for read datas ( home/alice/bash -> fs/home/alice/bash )
function read_exceptions( $excep_file, $prefix )
{
   $exceptions = array();
   $uid = array();
   $gid = array();
   $stat = @ stat( $excep_file );
   if ( ! $stat )
      fprintf(STDERR, "pas d'exceptions\n");
   else
   {
      $fd = fopen( $excep_file, "r" );
      while (($ligne = fgets($fd)) !== false)
      {

         $ligne = trim( $ligne );   // suppress useless space
         // don't parce comment lines
         if ( ( substr( $ligne, 0, 1 ) == "#" ) || ( substr( $ligne, 0, 2 ) == "//" ) )
            continue;
         $t = explode( " ", $ligne );

         if ( @ $t[0] == "herit" )
         {
           if ( count($t) == 5 )
           {
              // print ("analyse $ligne " . $t[1] . "\n");
              if ( $t[1] == "uid" )
                 $uid[ $prefix . DIRECTORY_SEPARATOR . $t[3] ] = $t[4];
              else if ( $t[1] == "gid" )
                 $gid[ $prefix . DIRECTORY_SEPARATOR . $t[3] ] = $t[4];
           }
         }
         else if ( count( $t ) == 2 )
         {
            // check there is 2 columns => file -> .file
            $exceptions[ $prefix . DIRECTORY_SEPARATOR . $t[0] ] = $t[1];
         }
         else
            fwrite (STDERR, $ligne . "not parsed");
      }
      fclose ($fd);
   }
   return array( $exceptions, $uid, $gid );
}

function dump_exceptions( $e )
{
   if ( ! empty( $e ) )
   {
      while ( list( $k, $v ) = each( $e ) )
      {
         fwrite ( STDERR, $k . " --> " . $v . "\n");
      }
      fwrite( STDERR, "-------------------------------\n");
  }
}

function radical_search( $needle, $tab )
{
   $res = array( false, 0);

   // decoupe la chaine $needle (séparateur /) pour rechercher si la chaine est heritee
   $t = explode( DIRECTORY_SEPARATOR, $needle );
   $s = "";
   for ( $i = 0; $i < count($t); $i++)
   {
      if ( strlen($s) != 0 )
         $s .= DIRECTORY_SEPARATOR;
      $s .= $t[$i];
      if ( array_key_exists( $s, $tab ) )
      {
        $res = array( true, $tab[ $s ] );
        $i = count($t);
     }
   } 

   return $res;
}

// return string formed by n spaces
function space( $n )
{
   $sp="";
   for ( $i = 0; $i<$n; $i++)
      $sp.= " ";
   return $sp;
}

function dirToArray($dir, $decalage)
{ 
   global $exceptions, $tabUID, $tabGID, $prefix;

   $result = "";

   // $result .= "Analyse le repertoire : $dir\n";

   $cdir = @ scandir($dir);
   if ( $cdir )
   {
      
      // parse les fichiers en 1er
      foreach ($cdir as $key => $filename) 
      { 
         $lname = $dir . DIRECTORY_SEPARATOR . $filename;
         if ( ! is_dir($lname) )
         {
            if ( is_link($lname) )
            {
               $result .= space( $decalage );
               $result.= '{ "name":"' . $filename . '", "mode":"120777", "path":"' . readlink( $lname ) . '"},' . "\n";
            }
            else
            {
               $ext = substr( $filename, strlen( $filename ) - 4, 4 );
               $infos = stat( $dir . DIRECTORY_SEPARATOR . $filename );
               // print ("traite le fichier : $dir" . DIRECTORY_SEPARATOR . "$filename\n");
               // file is compressed ... need to find the real (decompressed) size
               if ( $ext == ".bz2" )
               {
                 $l = 0;
                 $fh = bzopen($lname ,'r');
                 while(!feof($fh))
                 {
                    $buffer = bzread($fh);
                    if($buffer === FALSE)
                    {
                      $l=0;
                      break;
                    }
                    else if(bzerrno($fh) !== 0)
                    {
                      // die('Compression Problem');
                      $l=0;
                      break;
                    }
                    else
                      $l += strlen( $buffer );
                  }
                  bzclose($fh);
                  $d = sprintf("%04o", $infos['mode']);
                  $nname = substr( $filename, 0, strlen( $filename) - 4 );
                  $extra = "";
                  $needle = $dir . DIRECTORY_SEPARATOR . $nname;
                  if ( array_key_exists( $needle, $exceptions ) )
                  {
                     $extra = ', "src":"' . $needle . '" ';
                     $nname = basename( $exceptions[$needle] );
                  }
                  list( $res, $u ) = radical_search( $needle, $tabUID );
                  if ( $res )
                  {
                     fprintf (STDERR, "recherche $needle dans UID : trouvee ($u)\n");
                     $extra .= ', "uid":' . $u . " ";
                  }
                  list( $res, $g ) = radical_search( $needle, $tabGID );
                  if ( $res )
                  {
                     fprintf (STDERR, "recherche $needle dans GID : trouvee ($g)\n");
                     $extra .= ', "gid":' . $g . " ";
                  }

                  $result .= space( $decalage );
                  $result.= '{ "name":"' . $nname . '", "mode":"' . $d . '", "size":' . $l . ', "c":1' . $extra . '},' . "\n";
               }
               else
               {
                  // flat file
                  $result .= space( $decalage );
                  if ( ! $infos )
                     $result .= '{ "name":"' . $filename . '", "mode":"100644", "size":0},' . "\n";
                  else
                  {
                     $t=decoct($infos['mode'] & 000777);
                     $d = sprintf("%04o", $infos['mode']);
                     $extra = "";
                     $needle = $dir . DIRECTORY_SEPARATOR . $filename;
if ( $filename == "bashrc" )
{
   fwrite( STDERR, "fichier bashrc trouvé\n");
   fwrite( STDERR, "needle : $needle\n");
   if ( array_key_exists( $needle, $exceptions ) )
   {
      fwrite( STDERR, "needle founded\n");
      fwrite( STDERR, substr( $needle, strlen( $prefix ) + 1 ) . " -- " . basename( $exceptions[$needle] ) . "\n");
   }
   else
      fwrite( STDERR, "needle not founded\n");
}
// fprintf (STDERR, "cherche needle : $needle\n");
                     list( $res, $u ) = radical_search( $needle, $tabUID );
                     if ( $res )
                     {
                        fprintf (STDERR, "recherche $needle dans UID : trouvee ($u)\n");
                        $extra .= ', "uid":' . $u . " ";
                     }
                     list( $res, $g ) = radical_search( $needle, $tabGID );
                     if ( $res )
                     {
                        fprintf (STDERR, "recherche $needle dans GID : trouvee ($g)\n");
                        $extra .= ', "gid":' . $g . " ";
                     }
                     if ( array_key_exists( $needle, $exceptions ) )
                     {  
                        $filename = basename( $exceptions[$needle] );
                        $needle = substr( $needle, strlen( $prefix ) + 1 );
                        $extra .= ', "src":"' . $needle . '" ';
                     }

                     $result.= '{ "name":"' . $filename . '", "mode":"' . $d . '", "size":' . $infos["size"] . $extra . '},' . "\n";
                  }
               }
            }
         }
      }

      // parse les repertoire en 2nd
      foreach ($cdir as $key => $value)
      {
         if ( ! in_array($value,array(".",".."))) 
            if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
            {
               // $result .= space( $decalage) . $dir . DIRECTORY_SEPARATOR . $value . "-\n";
               // $result .= $dir . DIRECTORY_SEPARATOR . $value;
               $complement = "";
               $needle = $dir . DIRECTORY_SEPARATOR . $value;
               list( $res, $u ) = radical_search( $needle, $tabUID );
               if ( $res )
               {
                  fprintf (STDERR, "recherche $needle dans UID : trouvee ($u)\n");
                  $complement .= '"uid":' . $u . ", ";
               }
               list( $res, $g ) = radical_search( $needle, $tabGID );
               if ( $res )
               {
                  fprintf (STDERR, "recherche $needle dans GID: trouvee ($g)\n");
                  $complement .= '"gid":' . $g . ", ";
               }

               $result .= space( $decalage) . '{ "name":"' . $value . '", ' . $complement . '"mode":"40755", "child":[' . "\n";
               $b = rtrim( dirToArray($dir . DIRECTORY_SEPARATOR . $value, $decalage+3) );
               if ( strlen ($b) > 0 )
                  $result .= $b . "\n";
               $result .= space( $decalage) . "]},\n";
            } 
      }
      if ( substr( $result, strlen( $result ) - 2, 2 ) == ",\n" )
          $result = substr( $result, 0, strlen( $result ) - 2) . "\n";
   }

   // $result .= "Fin de l'analyse le repertoire : $dir\n";

   return $result;
}

// prefix : GLOBAL variable
$prefix = @ $argv[1];

// exceptions : GLOBAL Variable
list($exceptions, $tabUID, $tabGID ) = read_exceptions( "exceptions.txt", $prefix );
dump_exceptions( $exceptions );
dump_exceptions( $tabUID );

$decalage = 3;
$s  = '{"src":"../sys/fs", "fs":[' . "\n";
$s .= dirToArray( $prefix, $decalage );
$s .= "]}\n";

print ($s);

?>
