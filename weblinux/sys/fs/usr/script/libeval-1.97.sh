#!/bin/bash
# Auteur: Pascal Anelli
# 18 juin 2019

log="$(mktemp)"
res=0

#===================================================================
function xfinish
{
  echo "";
  echo "Interruption de l evaluateur";
  exit 1;
}

#===================================================================


trap xfinish SIGINT SIGTERM

# global var: challenge, log, rep, scriptname
function firstcheck() {
   echo "** Evaluation du challenge  $challenge **" | tee $log
   step false "debut retour true ou"

   if [ ! -f $rep/$scriptname ] || [ ! -x $rep/$scriptname ]
   then
        echo "Attention: Votre script doit s'appeler $scriptname, etre dans le repertoire $rep et etre executable" >&2
        exit 1
    fi
}

function vraifaux() {
    if test "$1" -eq  0 ; then
	echo "true"
    else
	echo "false"
    fi
}

# argument 1: cmd or (true, false) (State for this step) 
# argument 2: Associated message to step
# argument 3: Optional, true for a fatal error = exit from eval 
# step must be true to continue
function step() {
	local fatal=${3:-false}
        local etat=non

	# step true ou false
	$1  && { etat="oui" ; ((res++)) ; } #number of steps good
     
	echo  "step: $res"  "$2 $etat" |tee -a $log
	if $fatal && ! $1 ; then
	  echo " ERREUR Fatale "
	  exit 1
	fi
}


# argument 1: String to check
# return true if the  argument is an integer
function isinteger() {
 [[ "$1" =~ ^-?[0-9]+$ ]]
}


# argument 1: the string to hash by md5sum
function status() {
echo "Le code de l'evaluation a saisir est : " $(echo "$1" | md5sum| cut  -c4-6) 
}
#=====================================================================

