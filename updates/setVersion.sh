#!/bin/bash

# Xavier NICOLAY
# 1er Octobre 2020
# Université de La Réunion / LIM
# Usage : setVersion ../index.html
# -> Recherche la version courante (3.02) et propose de la modifier.

file=$1

if [ -f ${file} ]; then
   av=$( grep versionWeblinux ${file} | sed -e 's/.*<\/font>//' -e 's/<\/span>.*//' )
   echo "Version actuelle : -->${av}<--"
   echo -n "Entrez la version souhaitée : "
   read nv
   mv ${file} /tmp/index.$$
   cat  /tmp/index.$$ | sed -e 's/\(.*\)versionWeblinux\(.*\)<\/font>.*$/\1versionWeblinux\2<\/font>'${nv}'<\/span><\/td>/' > ${file}
   av=$( grep versionWeblinux ${file} | sed -e 's/.*<\/font>//' -e 's/<\/span>.*//' )
   echo "version set to ${av}"
   rm /tmp/index.$$
else
   echo "${file} not found"
fi

