#!/usr/bin/env php
<?php

$version="1.0";

// usage  : ./create_fs_json.php
// example: ./create_fs_json.php -d fs -o=fs.json -c=exceptions.txt

// parse $excep_file file to memorise file to transform (bashrc -> .bashrc)
// $prefix is a prefix for read datas ( home/alice/bash -> fs/home/alice/bash )
function read_exceptions( $excep_file, $prefix )
{
   $exceptions   = array();
   $uid          = array();
   $gid          = array();
   $ignoreFiles  = array();
   $ignoreDirs   = array();
   $chmodFiles   = array();
   $chmodDir     = array();
   $chmodDirName = array();
   $links        = array();
   $mesInfos     = array();
   $stat = @ stat( $excep_file );
   if ( ! $stat )
      fprintf(STDERR, "pas d'exceptions" . PHP_EOL);
   else
   {
      $fd = fopen( $excep_file, "r" );
      while (($ligne = fgets($fd)) !== false)
      {

         $ligne = trim( $ligne );   // suppress useless space
         // don't parce comment lines
         if ( ( substr( $ligne, 0, 1 ) == "#" ) || ( substr( $ligne, 0, 2 ) == "//" ) )
            continue;
         $ligne = $output = preg_replace('!\s+!', ' ', $ligne);  // replace multiple spaces by one space
         $t = explode( " ", $ligne );

         if ( @ $t[0] == "herit" )
         {
            if ( count($t) == 5 )
            {
               // print ("analyse $ligne " . $t[1] . "\n");
               if ( $t[1] == "uid" )
                  $uid[ $prefix . DIRECTORY_SEPARATOR . $t[3] ] = $t[4];
               else if ( $t[1] == "gid" )
                  $gid[ $prefix . DIRECTORY_SEPARATOR . $t[3] ] = $t[4];
            }
            else
            {
               $nn = count($t);
               fwrite (STDERR, "-->" . $ligne . "<-- not parsed : $nn arguments" . $PHP_EOL);
            }
         }
         else if ( @ $t[0] == "rename" )
         { 
            if ( count($t) == 3 )
            {  
               $exceptions[ $prefix . DIRECTORY_SEPARATOR . $t[1] ] = $t[2];
            }
            else
            {
               $nn = count($t);
               fwrite (STDERR, "-->" . $ligne . "<-- not parsed : $nn arguments" . $PHP_EOL);
            }
         }
         else if ( @ $t[0] == "forceload" )
         {
            $mesInfos["forceload"] = 1;
         }
         else if ( @ $t[0] == "webfs" )
         {
            if ( count($t) == 2 )
            { 
               if ( $t[1] == "relative" )
                  $mesInfos["forcerelative"] = 1;
               elseif ( $t[1] == "absolute" )
                  $mesInfos["forcerelative"] = 0;
               else
                  fwrite (STDERR, "-->" . $ligne . "<-- not parsed" . $PHP_EOL);
            }
            else if ( count($t) == 3 )
            { 
               if ( $t[1] == "path" )
                  $mesInfos["webpath"] = $t[2];
               else
                  fwrite (STDERR, "-->" . $ligne . "<-- not parsed" . $PHP_EOL);
            }
            else
               fwrite (STDERR, "-->" . $ligne . "<-- not parsed : $nn arguments" . $PHP_EOL);
         }
         else if ( @ $t[0] == "ignore" )
         {
            if ( count($t) == 3 )
            {
               if ( $t[1] == "file" )
                  array_push( $ignoreFiles, $t[2] );
               else if ( $t[1] == "dir" )
                  $ignoreDirs[ $t[2] ] = "ignore";
               else
                  fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
            }
            else
               fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
         }
         else if ( @ $t[0] == "forcemod" )
         {
           if ( count($t) == 4 )
           {
              if ( $t[1] == "dir" )
              {
                 $chmodDir [ $t[2] ] = $t[3];
              }
              elseif ( $t[1] == "rep" )
              {
                 $chmodDirName [ $t[2] ] = $t[3];
              }
              else if ( $t[1] == "file" )
              {
                 $chmodFiles [ $t[2] ] = $t[3];
              }
              else
                 fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
           }
           else
              fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
         }
         else if ( @ $t[0] == "mklink" )
         {
            if ( count($t) == 3 )
            {
               $dir = dirname( $t[1] );
               if ( substr( $dir,0,1 ) == DIRECTORY_SEPARATOR )
                  $dir = substr( $dir,1);
               $link = basename( $t[1] );
               $links[ $dir ][ $link ] = $t[2];
           }
           else
              fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
         }
         else if ( count( $t ) == 2 )
         {
            // check there is 2 columns => file -> .file
            $exceptions[ $prefix . DIRECTORY_SEPARATOR . $t[0] ] = $t[1];
         }
         else if ( strlen( $ligne ) > 0 )
            fwrite (STDERR, $ligne . " not parsed" . PHP_EOL);
         // no warning for empty lines
      }
      fclose ($fd);
   }
   $parse = array( "exceptions"   => $exceptions,
                   "uid"          => $uid,
                   "gid"          => $gid,
                   "ignoreFiles"  => $ignoreFiles,
                   "ignoreDirs"   => $ignoreDirs,
                   "chmodDir"     => $chmodDir,
                   "chmodDirName" => $chmodDirName,
                   "chmodFiles"   => $chmodFiles,
                   "links"        => $links,
                   "mesInfos"     => $mesInfos);
   return $parse;
   // return array( $exceptions, $uid, $gid, $ignoreFiles, $ignoreDirs, $chmodDir, $chmodDirName, $chmodFiles, $links, $mesInfos );
}

function dump_exceptions( $e )
{
   if ( ! empty( $e ) )
   {
      while ( list( $k, $v ) = each( $e ) )
      {
         if ( is_array($v) )
         {
            foreach ( $v as $i=>$val )
            {
               fwrite ( STDERR, $k . "[" . $i . "] --> " . $val . PHP_EOL);
            }
         }
         else
            fwrite ( STDERR, $k . " --> " . $v . PHP_EOL);
      }
      fwrite( STDERR, "-------------------------------" . PHP_EOL);
  }
}

function radical_search( $needle, $tab )
{
   $res = array( false, 0);

   // decoupe la chaine $needle (séparateur /) pour rechercher si la chaine est heritee
   $t = explode( DIRECTORY_SEPARATOR, $needle );
   $s = "";
   for ( $i = 0; $i < count($t); $i++)
   {
      if ( strlen($s) != 0 )
         $s .= DIRECTORY_SEPARATOR;
      $s .= $t[$i];
      if ( array_key_exists( $s, $tab ) )
      {
        $res = array( true, $tab[ $s ] );
        $i = count($t);
     }
   } 

   return $res;
}

// return string formed by n spaces
// just to have a clean & beautiful presentation in the json file
function space( $n )
{
   $sp="";
   for ( $i = 0; $i<$n; $i++)
      $sp.= " ";
   return $sp;
}

function dirToArray($dir, $decalage)
{ 
   // global $exceptions, $tabUID, $tabGID, $prefix,$ignoreFiles, $ignoreDirs, $chmodDir, $chmodDirName, $chmodFiles, $prefix, $links, $mesInfos;
   global $fileparse;
   global $superverbose;
   global $prefix;

   $exceptions  = $fileparse["exceptions"];
   $tabUID      = $fileparse["uid"];
   $tabGID      = $fileparse["gid"];
   $ignoreFiles = $fileparse["ignoreFiles"];
   $ignoreDirs  = $fileparse["ignoreDirs"];
   $chmodDir    = $fileparse["chmodDir"];
   $chmodDirName= $fileparse["chmodDirName"];
   $chmodFiles  = $fileparse["chmodFiles"];
   $links       = $fileparse["links"];
   $mesInfos    = $fileparse["mesInfos"];

   $result = "";
   $lprefix = strlen( $prefix ) + 1;  // 1 = DIRECTORY_SEPARATOR

   // fwrite (STDERR, "___Analyse le repertoire : $dir ($prefix)\n");

   $cdir = @ scandir($dir);
   if ( $cdir )
   {
      // parse les fichiers en 1er
      foreach ($cdir as $key => $filename) 
      { 
         $lname = $dir . DIRECTORY_SEPARATOR . $filename;
         $absoluteName = substr( $lname, $lprefix );
         $absoluteDir  = substr( $dir, $lprefix );
         if ( ! is_dir($lname) )
         {
            if ( $superverbose )
               fwrite (STDERR, "   Analyse $dir + $filename = $lname (asbDir=$absoluteDir :: absName=$absoluteName)" . PHP_EOL);
            $ext = substr( $filename, strlen( $filename ) - 4, 4 );
            $realfilename=$filename;
            if ( $ext == ".bz2" )
               $realfilename=substr($filename,0, strlen($filename)-4);
            if ( ( in_array( $filename, $ignoreFiles ) ) || ( in_array( $realfilename, $ignoreFiles ) ) )
            {
               if ( $superverbose )
                  fwrite (STDERR, "   +-> ignored" . PHP_EOL);
               continue;
            }
            else if ( is_link($lname) )
            {
               // Check if link is not already in Array(links)
               // fwrite (STDERR, "Check $lname :: $absoluteName :: $absoluteDir :: $filename\n");
               if ( ! isset( $links[ $absoluteDir] [ $filename ] ) )
                  $result .= space( $decalage ) . '{ "name":"' . $filename . '", "mode":"120777", "path":"' . readlink( $lname ) . '"},' . "\n";
            }
            else
            {
               $infos = stat( $dir . DIRECTORY_SEPARATOR . $filename );
               // print ("traite le fichier : $dir" . DIRECTORY_SEPARATOR . "$filename\n");
               // file is compressed ... need to find the real (decompressed) size
               if ( $ext == ".bz2" )
               {
                  $l = 0;
                  $fh = bzopen($lname ,'r');
                  while(!feof($fh))
                  {
                     $buffer = bzread($fh);
                     if($buffer === FALSE)
                     {
                       $l=0;
                       break;
                     }
                     else if(bzerrno($fh) !== 0)
                     {
                        // die('Compression Problem');
                        $l=0;
                        break;
                     }
                     else
                        $l += strlen( $buffer );
                  }
                  bzclose($fh);
                  $d = sprintf("%04o", $infos['mode']);
                  $nname = substr( $filename, 0, strlen( $filename) - 4 );      // substract the 4 last chars (".bz2")
                  $extra = "";
                  $needle = $dir . DIRECTORY_SEPARATOR . $nname;
                  if ( array_key_exists( $needle, $exceptions ) )
                  {
                     $extra = ', "src":"' . $needle . '" ';
                     $nname = basename( $exceptions[$needle] );
                  }
                  list( $res, $u ) = radical_search( $needle, $tabUID );
                  if ( $res )
                  {
                     if ( $superverbose )
                        fprintf (STDERR, "   +-> search $needle in Array(UID) : FOUNDED ($u)" . PHP_EOL);
                     $extra .= ', "uid":' . $u;
                  }
                  list( $res, $g ) = radical_search( $needle, $tabGID );
                  if ( $res )
                  {
                     if ( $superverbose )
                        fprintf (STDERR, "   +-> search $needle in Array(GID) : FOUNDED ($g)" . PHP_EOL);
                     $extra .= ', "gid":' . $g;
                  }
                  if ( isset( $chmodDir[ $absoluteDir ] ) )
                  {
                     if ( $superverbose ) 
                        fprintf (STDERR, "   +-> from chmodDir " . PHP_EOL);
                     $d = $chmodDir[ $absoluteDir ];
                  }

                  $rawFileName = substr( $absoluteName, 0, strlen( $absoluteName ) - 4); // substract the 4 last chars (".bz2")
                  if ( isset( $chmodFiles[ $rawFileName ] ) )
                  {
                     if ( $superverbose )
                        fprintf (STDERR, "   +-> search $rawFileName in chmodFiles : FOUNDED" . PHP_EOL);
                     $d = $chmodFiles[ $rawFileName ];
                  }

                  if ( isset( $mesInfos["forceload"] ) )
                     $extra .= ', "load":1';

                  $result .= space( $decalage );
                  $result.= '{ "name":"' . $nname . '", "mode":"' . $d . '", "size":' . $l . ', "c":1' . $extra . '},' . "\n";
               }
               else
               {
                  // flat file
                  $result .= space( $decalage );
                  if ( ! $infos )
                     $result .= '{ "name":"' . $filename . '", "mode":"100644", "size":0},' . "\n";
                   else
                  {
                     $t=decoct($infos['mode'] & 000777);
                     $d = sprintf("%04o", $infos['mode']);
                     $extra = "";
                     $needle = $dir . DIRECTORY_SEPARATOR . $filename;
                     list( $res, $u ) = radical_search( $needle, $tabUID );
                     if ( $res )
                     {
                        if ( $superverbose )
                           fprintf (STDERR, "   +-> search $needle in Array(UID) : FOUNDED ($u)\n");
                        $extra .= ', "uid":' . $u;
                     }
                     list( $res, $g ) = radical_search( $needle, $tabGID );
                     if ( $res )
                     {
                        if ( $superverbose )
                           fprintf (STDERR, "  +-> search $needle in Array(GID) : FOUNDED ($g)\n");
                        $extra .= ', "gid":' . $g;
                     }
                     if ( isset( $chmodDir[ $absoluteDir ] ) )
                     {
                        if ( $superverbose )
                           fprintf (STDERR, "   +-> from chmodDir " . PHP_EOL);
                        $d = $chmodDir[ $absoluteDir ];
                     }

                     if ( isset( $chmodFiles[ $absoluteName ] ) )
                     {
                        if ( $superverbose )
                           fprintf (STDERR, "   +-> search $absoluteName in chmodFiles : FOUNDED" . PHP_EOL);
                        $d = $chmodFiles[ $absoluteName ];
                     }

                     if ( array_key_exists( $needle, $exceptions ) )
                     {  
                        $filename = basename( $exceptions[$needle] );
                        $needle = substr( $needle, strlen( $prefix ) + 1 );
                        $extra .= ', "src":"' . $needle . '" ';
                     }
                     if ( isset( $mesInfos["forceload"] ) )
                        $extra .= ', "load":1';
                     $result.= '{ "name":"' . $filename . '", "mode":"' . $d . '", "size":' . $infos["size"] . $extra . '},' . "\n";
                  }
               }
            }
         }
      }

      // parse les repertoires en 2nd
      foreach ($cdir as $key => $value)
      {
         if ( ! in_array($value,array(".",".."))) 
            if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
            {
               // $result .= space( $decalage) . $dir . DIRECTORY_SEPARATOR . $value . "-\n";
               // $result .= $dir . DIRECTORY_SEPARATOR . $value;
               $complement = "";
               $needle = $dir . DIRECTORY_SEPARATOR . $value;
               $absoluteDir  = substr( $dir, $lprefix );
               if ( strlen($absoluteDir) > 0 )
                  $absoluteDir .= DIRECTORY_SEPARATOR;
               $absoluteDir .= $value;
               if ( isset( $ignoreDirs[ $absoluteDir ] ) )
               {
                  if ( $superverbose )
                     fprintf (STDERR, "   +-> IGNORE DIR $absoluteDir\n");
                  continue;
               }
               list( $res, $u ) = radical_search( $needle, $tabUID );
               if ( $res )
               {
                  if ( $superverbose )
                     fprintf (STDERR, "   +-> search $needle in Array(UID) : FOUNDED ($u)\n");
                  $complement .= '"uid":' . $u . ", ";
               }
               list( $res, $g ) = radical_search( $needle, $tabGID );
               if ( $res )
               {
                  if ( $superverbose )
                     fprintf (STDERR, "   +-> search $needle in Array(GID) : FOUNDED ($g)\n");
                  $complement .= '"gid":' . $g . ", ";
               }
               
               $mode = "40755";
               if ( isset($chmodDirName[ $absoluteDir] ))
               {
                  $mode = $chmodDirName[ $absoluteDir];
                  if ( $superverbose )
                     fprintf (STDERR, "   +-> Change Mode(dir) : $value -> $mode\n");
               }

               $result .= space( $decalage) . '{ "name":"' . $value . '", ' . $complement . '"mode":"' . $mode . '", "child":[' . "\n";

               // print ("ANALYSE $dir + $value --> $absoluteDir\n");
               if ( isset( $links[ $absoluteDir ] ) )
               {
                  // Put all links first
                  if ( $superverbose )
                     fwrite (STDERR, "   +-> Create links for $absoluteDir" . PHP_EOL);
                  foreach ( $links[ $absoluteDir ] as $link => $dest )
                  {
                     if ( $superverbose )
                        fwrite (STDERR, "      - $link -> $dest" . PHP_EOL );
                     $result .= space( $decalage + 3) . '{ "name":"' . $link . '", "mode":"120777", "path":"' . $dest . '"},' . "\n";
                  }
               }
               $b = rtrim( dirToArray($dir . DIRECTORY_SEPARATOR . $value, $decalage+3) );
               if ( strlen ($b) > 0 )
                  $result .= $b . "\n";
               $result .= space( $decalage) . "]},\n";
            } 
      }
      if ( substr( $result, strlen( $result ) - 2, 2 ) == ",\n" )
         $result = substr( $result, 0, strlen( $result ) - 2) . "\n";
   }

   // $result .= "Fin de l'analyse du repertoire : $dir" . PHP_EOL;

   return $result;
}

function usage($scriptName)
{
   print ("$scriptName [-v] [-vv] [-r] -d path [-o=outputfile] [-c=consignes.txt]" . PHP_EOL);
   print (" -x  : print Version" . PHP_EOL);
   print (" -v  : verbose" . PHP_EOL);
   print (" -vv : more verbose" . PHP_EOL);
   print (" -d path : analyse path" . PHP_EOL);
   print (" -o=filename : write result in the filename" . PHP_EOL);
   print (" -ofilename  : write result in the filename" . PHP_EOL);
   print (" -c  : configuration file (default: exceptions.txt)" . PHP_EOL);
   print (" -p=myPath : path of the filesystem (default: fs)" . PHP_EOL);
}
   
/**************************************************************************************************************************
 *                                                          MAIN                                                          *
 **************************************************************************************************************************/
// Init global variables
$verbose      = false;
$superverbose = false;
$relatif      = false;
$fileConf     = "exceptions.txt";
$path         = "sys/fs";

$shortopts  = "";
$shortopts .= "d:";  // Valeur requise
$shortopts .= "v::"; // Valeur optionnelle
$shortopts .= "o::"; // Valeur optionnelle
$shortopts .= "c::"; // Valeur optionnelle
$shortopts .= "p::"; // Valeur optionnelle
$shortopts .= "x"; // autosuffisant

$longopts  = array(
    "required:",     // Valeur requise
    "optional::",    // Valeur optionnelle
    "optional::",    // Valeur optionnelle
    "optional::",    // Valeur optionnelle
    "optional::",     // Valeur optionnelle
    "opt",     // Valeur optionnelle
);

$options = getopt($shortopts, $longopts);

if ( isset( $options["x"] ) )
{
   die("Version : $version". PHP_EOL);
}

if ( isset( $options["d"] ) )
   $prefix = $options["d"];
else
{
   usage( $argv[0] );
   die("ERROR: -d directory is necessay". PHP_EOL);
}
if ( isset( $options["v"] ) )
{
   if ( $options["v"] == "v" )
      $superverbose=true;
   else
      $verbose=true;
}

if ( isset( $options["c"] ) )
   $fileConf = $options["c"];

// var_dump( $options ); exit;

// exceptions, ... : GLOBAL Variables
$fileparse = read_exceptions( $fileConf, $prefix );
// list($exceptions, $tabUID, $tabGID, $ignoreFiles, $ignoreDirs, $chmodDir, $chmodDirName, $chmodFiles, $links, $mesInfos ) = read_exceptions( $fileConf, $prefix );
if ( ( $verbose ) || ( $superverbose ) )
{
   dump_exceptions( $fileparse["exceptions"] );
   dump_exceptions( $fileparse["uid"] );
   dump_exceptions( $fileparse["gid"] );
   dump_exceptions( $fileparse["ignoreFiles"] );
   dump_exceptions( $fileparse["ignoreDirs"] );
   dump_exceptions( $fileparse["chmodDir"] );
   dump_exceptions( $fileparse["chmodDirName"] );
   dump_exceptions( $fileparse["chmodFiles"] );
   dump_exceptions( $fileparse["links"] );
   dump_exceptions( $fileparse["mesInfos"] );
}

if ( isset( $fileparse["mesInfos"]["webpath"] ) )
   $path = $fileparse["mesInfos"]["webpath"]; // Je prends tout le chemin
if ( isset( $options["p"] ) )
   $path = "sys/" . $options["p"]; // je préfixe le chemin passé en paramètre

$prefixPath = DIRECTORY_SEPARATOR;
if ( @ $fileparse["mesInfos"]["forcerelative"] == 1 )
   $prefixPath = "";
$path = $prefixPath . $path;

$decalage = 3;
$s  = '{"src":"' . $path . '", "fs":[' . "\n";
$s .= dirToArray( $prefix, $decalage );
$s .= "]}\n";

if ( isset( $options["o"] ) )
{
   $filename = $options["o"];
   $fd = fopen( $filename, "w" );
   if ( $fd )
   {
      fwrite( $fd, $s );
      fclose($fd);
   }
   else
   {
      fprintf( STDERR, "Can't open $filename" . PHP_EOL );
      print($s);
   }
}
else
{
   // print output on sdtout
   print ($s);
}

?>
