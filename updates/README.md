To update the 2 filesystems (fs & basefs)
just run the php script : create_fs_json.php (whith php-cli, just add execute rights) with the right arguments.

* Step 1:
in the "sys" directory, execute :
`./create_fs_json.php -d fs -o=fs.json`

* Step 2:
in the "sys/or1k" directory, execute :
`./create_fs_json.php -d basefs -o=basefs.json`

that's all folks !

To change the version printed on the web page, you can execute the script `setVersion.sh`.

***********************************************************
Pour réaliser la mise à jour des 2 systèmes de fichiers,
exécutez simplement le script create_fs_json.php (installez php-cli, et donnez les droits en exécution) avec les bons arguments.

* Etape 1:
dans le répertoire "sys", lancez :
`./create_fs_json.php -d fs -o=fs.json`

* Etape 2:
dans le répertoire "sys/or1k", lancez :
`./create_fs_json.php -d basefs -o=basefs.json`

that's all folks !

Pour modifier la version affichée sur la page web, vous pouvez lancer le script `setVersion.sh`
